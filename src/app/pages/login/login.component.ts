import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ],
})
export class LoginComponent implements OnInit {
  public checked = false;
  public loginForm!: FormGroup;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
  ) {
    this.checked = this.authService.isAuth;
  }

  public get label(): string {
    return this.checked ? 'Logout' : 'Login';
  }

  update($event: Event) {
    this.checked = !this.checked;
    this.authService.isAuth = this.checked;
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [ '', Validators.required ],
      password: [ '', Validators.required ],
    });
  }

  public onSubmit() {
    if (this.loginForm.invalid) { return; }
    console.log(this.loginForm.value);
    this.authService.login(this.loginForm.value);
  }

  public get isLoggedIn() {
    return this.authService.isAuth;
  }
}
