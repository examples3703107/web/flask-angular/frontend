import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ],
})
export class HomeComponent {
  public content: string = '';

  constructor(private api: ApiService) {
    this.api.home.subscribe({
      next: value => this.content = value,
      error: err => {
        this.content = 'some error occurred';
        console.error(err);
      },
    });
  }
}
