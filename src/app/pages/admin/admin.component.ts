import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [ './admin.component.scss' ],
})
export class AdminComponent {
  public content: string = '';

  constructor(private api: ApiService) {
    this.api.admin.subscribe({
      next: value => this.content = value,
      error: err => {
        this.content = 'something went wrong';
        console.error(err);
      },
    });
  }

}
