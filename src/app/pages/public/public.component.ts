import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: [ './public.component.scss' ],
})
export class PublicComponent {
  public content: string = '';

  constructor(private api: ApiService) {
    this.api.public.subscribe({
      next: value => this.content = value,
      error: err => {
        this.content = 'some error occurred';
        console.error(err);
      },
    });
  }

}
