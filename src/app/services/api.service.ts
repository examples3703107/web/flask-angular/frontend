import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private readonly base_uri = 'http://localhost:5000';

  constructor(private http: HttpClient) { }

  public get home() {
    return this.http.get(this.uri('/'), { responseType: 'text' });
  }

  public get public() {
    return this.http.get(this.uri('/v1/public'), { responseType: 'text' });
  }

  public login(user: any) {
    const credentials = btoa(`${user.username}:${user.password}`);
    const headers = new HttpHeaders().set('Authorization', `Basic ${credentials}`);
    return this.http.post(this.uri('/login'), {}, { headers });
  }

  public get admin() {
    const token = sessionStorage.getItem('token') || '';
    const headers = new HttpHeaders().set('x-access-tokens', token);
    return this.http.get(this.uri('/v1/admin'), { headers, responseType: 'text' });
  }

  private uri(path: string): string {
    return this.base_uri + path;
  }
}
