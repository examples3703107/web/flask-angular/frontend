import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private api: ApiService) {
    this._isAuth = Boolean(sessionStorage.getItem('loggedIn') || 'false');
  }

  private _isAuth = false;

  public get isAuth() {
    const token = sessionStorage.getItem('token');
    if (!token) {
      return this._isAuth;
    }
    const decoded = jwtDecode(token) as any;
    console.log(decoded);
    return (decoded.exp && (decoded.exp > new Date().getTime() / 1000));
  }

  public set isAuth(value: boolean) {
    this._isAuth = value;
    sessionStorage.setItem('loggedIn', String(value));
    if (!this._isAuth) {
      this.logout();
    }
  }

  public get authStatus(): boolean {
    return this._isAuth;
  }

  public login(user: any) {
    this.api.login(user).subscribe({
      next: (value: any) => {
        if (value.token) {
          sessionStorage.setItem('token', value.token);
          this._isAuth = true;
          return true;
        }
        this._isAuth = false;
        return false;
      },
      error: console.error,
    });
  }

  public logout() {
    sessionStorage.removeItem('token');
    this._isAuth = false;
  }
}
